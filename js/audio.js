/*
*/
navigator.getUserMedia = navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia;

function audio_send_tool(verbose)
{
    if (typeof(verbose) == "undefined")
    {
	verbose = false;
    }
    var audio_tool = {files : []};
    audio_tool.onrejected = function(){};
    audio_tool.onaccepted = function(){};
    audio_tool.accepted = false;
    audio_tool.recording = false;
    audio_tool.onbeforesending = function(){};
    audio_tool.onNewFileAvailable = function(index)
    {
	if (verbose)
	    console.log("A new file is available at index:"+(index));
    };
    audio_tool.onRejectedRecording = function()
    {
	if (verbose)
	    console.log("Getusermedia get rejected");
    };
    audio_tool.onStartRecording = function()
    {
	if (verbose)
	    console.log("Start recording");
    };
    audio_tool.onStopRecording = function()
    {
	if (verbose)
	    console.log("Stop recording");
    };
    audio_tool.onStartSending = function()
    {
     	if (verbose)
	    console.log("Start sending files");
    };
    audio_tool.onFilesSent = function(response)
    {
	if (verbose)		   
	    console.log("Files sent ("+response+")");	
    };
    audio_tool.onFileSent = function(response){
	if (verbose)
	    console.log("File sent ("+response+")");
    };

    audio_tool.onFileError = function(evt)
    {
	if (verbose)
	    console.log("Error during file(s) upload");
    };

    audio_tool.reject_callback = function(err)
    {
	this.onrejected();
	console.log(err.name + ": " + err.message);
    };
    audio_tool.success_callback = function(stream)
    {
	this.onaccepted();
	this.accepted = true;
	this.mr = new MediaRecorder(stream);
	this.mr.audio_tool = this;
	this.mr.ondataavailable = function(e)
	{
	    this.audio_tool.files.push(new Blob([e.data]));
	    this.audio_tool.onNewFileAvailable(this.audio_tool.files.length-1);
	};
    };
    navigator.mediaDevices.getUserMedia({audio:true, video:false}).then(
	function(stream)
	{
	    audio_tool.success_callback(stream);
	}).catch(
	    function(err)
	    {	
		audio_tool.reject_callback(err)
	    });
    audio_tool.sendAllFiles = function(extra_values)
    {
	if (typeof(extra_values) == "undefined")
	{
	    var extra_values = [];
	};
	var obj = new FormData();
	for (var i=0; i < extra_values.length; i++)
	    obj.append(values[i]["key"], values[i]["value"]);
	for (var i=0; i < this.files.length; i++)
	    obj.append("audiofile_"+i, this.files[i]);
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("POST", "request.php", true);
	xmlhttp.audio_tool = this;
	xmlhttp.addEventListener("load",
				 function()
				 {
				     this.audio_tool.onFilesSent(this.responseText);
				 });
	
	xmlhttp.send(obj);    
    };
    
    audio_tool.sendFile = function(index, extra_values)
    {
	if (typeof(extra_values) == "undefined")
	{
	    var extra_values = [];
	};
	var obj = new FormData();
	for (var i=0; i < extra_values.length; i++)
	    obj.append(values[i]["key"], values[i]["value"]);
	obj.append("audiofile", this.files[index]);
	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("POST", "request.php", true);
	xmlhttp.audio_tool = this;
	xmlhttp.addEventListener("load",
				 function()
				 {
				     this.audio_tool.onFileSent(this.responseText);
				 });
	xmlhttp.addEventListener("error", function(evt)
				 {
				     this.audio_tool.onFileError(evt);
				 });
	
	
	xmlhttp.send(obj);    
    };

    audio_tool.start = function()
    {
	if (this.accepted)
	{
	    this.mr.start();
	    this.recording = true;
	    this.onStartRecording();
	}
	else
	{
	    this.onRejectedRecording();
	}
    };
    audio_tool.stop = function()
    {
	if (this.recording)
	{
	    this.recording = false;
	    this.mr.stop();
	    this.onStopRecording();
	}
    };
    audio_tool.getSrcFile = function(index)
    {
	return URL.createObjectURL(this.files[index]);
    };
    return audio_tool;
}

