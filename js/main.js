function load_audio()
{
    var contener = document.createElement("div");
    var at = audio_send_tool(true);
    var ico = document.createElement("img");
    ico.at = at;
    ico.src = "image/mic.png";
    ico.setAttribute("class", "mic");
    contener.appendChild(ico);
    ico.started = false;    
    ico.start = function()
    {
	this.started = true;
	this.setAttribute("class", "mic_active");
	this.at.start();
    }
    ico.stop = function()
    {
	this.started = false;
	this.setAttribute("class", "mic");
	this.at.stop();
    };
    ico.onclick = function()
    {
	if (!this.started)
	{
	    this.start();
	}
	else
	{
	    this.stop();
	}
    };
    var upload = document.createElement("input");
    upload.value = "upload all recorded files";
    upload.type = "button";
    upload.at = at;
    upload.onclick = function()
    {
	this.at.sendAllFiles();
    };    
    contener.appendChild(upload);
    at.onNewFileAvailable = function(index)
    {
	var audio_contener = document.createElement("div");
	audio_contener.setAttribute("class", "audio_contener");
	audio_contener.innerHTML = "File : "+index;
	var audio_reader = document.createElement("audio");
	audio_reader.controls = "controls";
	audio_contener.appendChild(audio_reader);
	var upload = document.createElement("input");
	upload.value = "Upload just this one";
	upload.type = "button";
	upload.index = index;
	upload.at = this;
	upload.onclick = function()
	{
	    this.at.sendFile(this.index);
	    this.disable=true;
	    this.at.curr_butt = this;
	    this.value = "Uploading ...";
	    this.at.onFileSent = function(reply)
	    {
		var obj = JSON.parse(reply);
		this.curr_butt.disable = false;
		this.curr_butt.value = "Re-upload it";
		var contener = this.curr_butt.parentElement;
		var direct_link = document.createElement("a");
		direct_link.innerHTML = " Direct link to audio file ";
		direct_link.href = "."+obj[""]["url"];
		contener.insertBefore(direct_link, this.curr_butt);
	    }
	};    
	
	audio_reader.src = this.getSrcFile(index);
	contener.appendChild(audio_contener);
	contener.appendChild(upload);
	
    };
    return contener;
}
document.body.appendChild(load_audio());
