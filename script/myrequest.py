from utils import parse_get, obj_to_Json, fresh_dir
import os
audio_path = "../data/audio"
format_to_conv = [ "mp3", "wav"]

def deal_request(s):
    _get_dic = parse_get(s)
    reply = {}
    new_dir = "{}/{}/".format(audio_path, fresh_dir(audio_path))
    for field in _get_dic:
        if field[:14] == "file:audiofile":            
             new_path = save_audio_file( field[14:],
                                ".." + _get_dic[field],
                                new_dir)
             reply[field[14:]] = {"url" : new_path, "format": format_to_conv}
             
    return obj_to_Json(reply)

def save_audio_file(field, file_path, dir_save):
    if len(field) == 0:
        field = "file"
    new_file_path = dir_save + field
    print(new_file_path)
    os.renames(file_path, new_file_path)
    for form in format_to_conv:
        os.system("avconv -i {} {} > /dev/null".format(new_file_path, new_file_path+"."+form))
    return new_file_path[3:]
    
    
      
