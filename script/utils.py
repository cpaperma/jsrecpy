import json
from urllib import parse
import random
import string
import os

alphabet = string.ascii_letters

def parse_get(s):
    t = s.split("?")[1].replace("+"," ").split("&")
    dic = {}
    for x in t:
        if len(x) > 0:
            y = x.split("=")
            y_left = parse.unquote(y[0])
            y_right = parse.unquote("=".join(y[1:len(y)]))
            dic[y_left] = y_right
    return dic        

def obj_to_Json(obj):
    return json.dumps(obj, default=lambda o: o.__dict__,sort_keys=True, indent=4)

def fresh_dir(path):
    rand_length = 5
    random_str = "".join(random.sample(alphabet, rand_length))
    while os.path.exists(path+"/"+random_str):
        random_str = random.sample(alphabet, rand_length)
    return random_str
