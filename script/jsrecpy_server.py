#! /usr/bin/python3
import multiprocessing
import time
from socketserver import ThreadingMixIn
from http.server import SimpleHTTPRequestHandler, HTTPServer
import threading
import sys
import os
from myrequest import deal_request
import urllib
pgms = [
    {
        "pgm_name" : "avconv",
        "error_msg": "install libav-tools"
    }   
]

for pgm in pgms:
    if not os.system("command -v {} > /dev/null".format(pgm["pgm_name"])) == 0:
        raise ValueError(pgm["error_msg"])
                     
                 
local = "localhost"
HOST_NAME = local
PORT_NUMBER = 5577 # Maybe set this to 9000.
date = time.asctime().replace(" ","").replace(":",".")
log = open("log/"+date,"w")

class MyHandler(SimpleHTTPRequestHandler):
    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
    def do_GET(s):
        """Respond to a GET request."""
        print("header", s.headers)
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
        s.wfile.write(deal_request(s.path).encode())
        log.write(s.path)
        print(s.path)
class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""        
 
httpd =  ThreadedHTTPServer((HOST_NAME, PORT_NUMBER), MyHandler)
print(time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER))
try:
    httpd.serve_forever()
except KeyboardInterrupt:    
    httpd.server_close()
    pass
    
print(time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))
log.close()
