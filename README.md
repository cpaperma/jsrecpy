# Javascript 
Javascript audio wrapup that *should* work on most of browser (to check)

The file "js/audio.js" contains the function *audio_send_tool* that 
provide an object wrapping up record sound and send them to "request.php" 
that will copy them in a directory tmp that **need** to allow www-data write
permission to work. On debian `chgrp www-data tmp; chmod g+w tmp` should 
do the trick. 

The usage of the wrapping is rather simple:  
```javascript
at = audio_send_tool(); //load the wrapping
at.start(); //start recording
at.stop(); //stop recording and add a blob file to at.files
at.sendFiles(extra_values); //send all files in at.files to server.
//extra_values is an optional argument that should an array of 
//{"key": some_key, "value": some_value} 
//to provide to the server.  
at.sendFile(index, extra_values); //send only file at index. 
at.getSrcFile(index); //return a local uri that can be use with audio element
```
It is also possible to play locally the recorded files with an audio element.

The file "js/main.js" provides a small illustration on how to use the audio object.
You can see this example working [here](https://paperman.name/jsrecpy)

The code is event based because audio need to be dealt in asynchronious way. 
Here the list of available event that can be use.

```javascript
onBeforeSending // triggered just before sending audio file
onNewFileAvailable(index) // triggered when a new sound file is available
onRejectedRecording //triggered when user reject audio permission
onStartRecording //triggered when object start recording
onStopRecording //triggered when object start recording
onStartSending // triggered when files are send to server
onFilesSent(response) //trigered when file are receive by server after sendFiles(). 
//The reply of the server is in response. 
//By default it is the concatenation of urls where the sound is stored.
//this behaviour can be change in request.php.
onFileSent(response) // same as before but for sendFile(); 
onFileError(evt) // event error when something went wrong during the transmission.
```

# PHP
The file request.php purpose is to transmit post request (including file upload) 
to python server. It you want some request only to be transmit, you should modify 
this file accordingly.

# Python server
The python server is listening HTTP GET query on localhost at port 5577. 
The basic behaviour is set in `myrequest.py`. It automatically saves audiofiles
to data folder, convert them to multiple format and return a JSON document containing 
the url and the available format.

More complexe behaviour can be done by modifying `myrequest.py`. It should not
be necessary to change the file `jsrecpy_server.py`.

Launch the python server just by executing jsrecpy_server.py.